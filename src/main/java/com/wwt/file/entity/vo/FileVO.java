package com.wwt.file.entity.vo;

import lombok.Data;

/**
 * 封装文件返回信息
 *
 */
@Data
public class FileVO {

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件id
     */
    private Long fileId;

    public FileVO(String fileName, Long fileId) {
        this.fileName = fileName;
        this.fileId = fileId;
    }
}
