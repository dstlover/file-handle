package com.wwt.file.entity.dto;

import cn.hutool.core.map.MapUtil;
import com.wwt.file.entity.User;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 封装前端传过来的用户请求信息
 */
@Data
public class UserExportDTO {

    private User user1;

    private User user2;

    private List<User> users;

    /**
     *
     * @return
     */
    public Map<String, Object> structDataMap() {
        Map<String, Object> map = MapUtil.createMap(HashMap.class);
        map.put("user1",this.user1);
        map.put("user2",this.user2);
        map.put("users",this.users);
        return map;
    }
}
