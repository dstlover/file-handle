package com.wwt.file.controller;

import com.wwt.file.entity.User;
import com.wwt.file.entity.dto.UserExportDTO;
import com.wwt.file.entity.vo.FileVO;
import com.wwt.file.result.DefaultResult;
import com.wwt.file.result.Result;
import com.wwt.file.result.enums.FileResultEnum;
import com.wwt.file.result.file.FileResult;
import com.wwt.file.service.IUserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private IUserService userService;

    @GetMapping("/hello")
    public Result hello(){
        return DefaultResult.success();
    }

    @PostMapping("/user")
    public Result user(User user){
        System.out.println(user);
        return DefaultResult.success();
    }


    @PostMapping("/export")
    public Result<FileVO> export(@RequestBody UserExportDTO upd, String type){
        return FileResult.setResult(FileResultEnum.UPLOAD_SUCCESS).data(userService.export(upd,type));
    }

}
