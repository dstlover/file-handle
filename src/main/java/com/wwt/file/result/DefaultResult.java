package com.wwt.file.result;

import com.wwt.file.result.enums.ResultCodeEnum;

/**
 * 默认响应结果
 * @param <T>
 */
public class DefaultResult<T> extends Result<T> {

    public static DefaultResult setResult(ResultCodeEnum resultCodeEnum){
        DefaultResult r = new DefaultResult();
        r.setSuccess(resultCodeEnum.getSuccess());
        r.setCode(resultCodeEnum.getCode());
        r.setMessage(resultCodeEnum.getMessage());
        return r;
    }

}
