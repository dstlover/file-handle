package com.wwt.file.result.file;

import com.wwt.file.result.Result;
import com.wwt.file.result.enums.FileResultEnum;

/**
 * 文件响应结果
 * @param <T>
 */
public class FileResult<T> extends Result<T> {

    public static FileResult setResult(FileResultEnum FileResultEnum){
        FileResult r = new FileResult();
        r.setSuccess(FileResultEnum.getSuccess());
        r.setCode(FileResultEnum.getCode());
        r.setMessage(FileResultEnum.getMessage());
        return r;
    }

}
