package com.wwt.file.result.enums;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum FileResultEnum {

    //只要不是20000都是失败
    SUCCESS(true, 20000,"成功"),
    UNKNOWN_REASON(false, 20001, "未知错误"),
    UPLOAD_SUCCESS(true, 20000, "文件上传成功"),
    UPLOAD_FAIL(false, 20002, "文件上传失败");

    private Boolean success;

    private Integer code;

    private String message;

    FileResultEnum(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }
}