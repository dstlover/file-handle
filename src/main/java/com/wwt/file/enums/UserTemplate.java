package com.wwt.file.enums;

/**
 * 用户模板
 */
public enum UserTemplate {
    USER_TEMPLATE_XML("user_template","用户信息","这是一个用户模板",FileTypeEnum.XML),
    USER_TEMPLATE_EXCEL("user_template","用户信息","这是一个用户模板",FileTypeEnum.EXCEL);

    /**
     * 模板名称
     */
    private String template;

    /**
     * 模板中文名
     */
    private String chName;

    /**
     * 详情
     */
    private String des;

    /**
     * 模板类型
     */
    private String type;

    /**
     * 模板后缀
     */
    private String suffix;


    UserTemplate(String template, String chName, String des, FileTypeEnum fileTypeEnum) {
        this.chName = chName;
        this.des = des;
        this.template = template + fileTypeEnum.getSuffix();
        this.type = fileTypeEnum.getType();
        this.suffix = fileTypeEnum.getSuffix();
    }

    public String getTemplate() {
        return template;
    }

    public String getChName() {
        return chName;
    }

    public String getDes() {
        return des;
    }

    public String getType() {
        return type;
    }

    public String getSuffix() {
        return suffix;
    }
}
