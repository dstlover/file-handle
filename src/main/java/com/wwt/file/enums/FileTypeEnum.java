package com.wwt.file.enums;

/**
 * 文件类型
 */
public enum  FileTypeEnum {
    WORD("word",".doc"),
    PDF("pdf",".pdf"),
    EXCEL("excel",".xlsx"),
    XML("xml",".xml")
    ;

    /**
     * 类型
     */
    private String type;

    /**
     * 后缀
     */
    private String suffix;

    FileTypeEnum(String type, String suffix) {
        this.type = type;
        this.suffix = suffix;
    }

    public String getType() {
        return type;
    }

    public String getSuffix() {
        return suffix;
    }
}
