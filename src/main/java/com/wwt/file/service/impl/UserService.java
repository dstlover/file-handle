package com.wwt.file.service.impl;

import cn.hutool.core.util.StrUtil;
import com.wwt.file.entity.dto.UserExportDTO;
import com.wwt.file.entity.vo.FileVO;
import com.wwt.file.enums.FileTypeEnum;
import com.wwt.file.enums.UserTemplate;
import com.wwt.file.service.ExportService;
import com.wwt.file.service.IUserService;
import com.wwt.file.util.excel.UserExcelDataWriter;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserService implements IUserService {

    @Resource
    public ExportService exportService;


    @Override
    public FileVO export(UserExportDTO upd, String type){
        //用工具类生成word文件
        if (StrUtil.equals(FileTypeEnum.WORD.getType(),type) || StrUtil.equals(FileTypeEnum.PDF.getType(),type)){
            return exportService.export(upd.structDataMap(), type, UserTemplate.USER_TEMPLATE_XML.getTemplate(),null);
        }
        return exportService.export(upd.structDataMap(), type, UserTemplate.USER_TEMPLATE_EXCEL.getTemplate(),new UserExcelDataWriter());
    }
}
