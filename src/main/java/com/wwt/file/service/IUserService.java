package com.wwt.file.service;

import com.wwt.file.entity.dto.UserExportDTO;
import com.wwt.file.entity.vo.FileVO;

public interface IUserService {

    public FileVO export(UserExportDTO upd, String type);

}
