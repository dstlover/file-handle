package com.wwt.file.service;

import cn.hutool.core.util.StrUtil;
import com.wwt.file.entity.vo.FileVO;
import com.wwt.file.enums.FileTypeEnum;
import com.wwt.file.enums.UserTemplate;
import com.wwt.file.util.DocUtil;
import com.wwt.file.util.excel.ExcelDataWriter;
import com.wwt.file.util.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

@Slf4j
@Service
public class ExportService {

    /**
     * 临时生成文件的路径
     */
    private static final String TEMP_FILE_DIR = "tmp\\";

    public FileVO export(Map<String, Object> dataMap, String type ,String tempFileName,ExcelDataWriter writer){
        if (StrUtil.equals(FileTypeEnum.WORD.getType(),type)){
            return new FileVO(UserTemplate.USER_TEMPLATE_XML.getChName(),exportWord(dataMap,tempFileName));
        }
        if (StrUtil.equals(FileTypeEnum.PDF.getType(),type)){
            return new FileVO(UserTemplate.USER_TEMPLATE_XML.getChName(),exportPdf(dataMap,tempFileName));
        }
        if (StrUtil.equals(FileTypeEnum.EXCEL.getType(),type)){
            return new FileVO(UserTemplate.USER_TEMPLATE_EXCEL.getChName(),exportExcel(dataMap,writer));
        }
        return new FileVO(null,null);
    }

    /**
     * @param dataMap        填充的数据,每个值都不能为null,可以为empty
     * @param tempFileName   模板文件名,放置在template目录下
     * @return 文件服务器返回的fileId
     * @description 模板导出word
     **/
    public Long exportWord(Map<String, Object> dataMap, String tempFileName) {

        //生成文件名
        String fileName = FileUtil.generateFileName(FileTypeEnum.WORD);
        DocUtil.saveWord(tempFileName, fileName, dataMap);
        //获取文件绝对路径
        String filePath = StrUtil.concat(true,FileUtil.getResourceBasePath(),TEMP_FILE_DIR,fileName);
        //上传文件到文件服务器
        //Long fileId = fileManagerService.uploadFile(filePath, fileName, "temp", ttl);
        //删除生成的文件
        //FileUtil.delDir(filePath);
        //模拟上传成功返回的id
        return 1234L; //fileId
    }

    /**
     * @param dataMap        填充的数据,每个值都不能为null,可以为empty
     * @param tempFileName   模板文件名,放置在template目录下
     * @return 文件服务器返回的fileId
     * @description 模板导出pdf
     **/
    public Long exportPdf(Map<String, Object> dataMap, String tempFileName) {
        //生成文件名
        String fileName = FileUtil.generateFileName(FileTypeEnum.PDF);
        DocUtil.savePdf(tempFileName, fileName, dataMap);
        String filePath = new File(FileUtil.getResourceBasePath(), TEMP_FILE_DIR + fileName).getAbsolutePath();
        //上传文件到文件服务器
        //Long fileId = fileManagerService.uploadFile(filePath, fileName, "temp", ttl);
        //FileUtil.delDir(filePath);
        return 1234L; //fileId
    }

    /**
     * 导出excel
     * 包括生成文档和上传文件
     * @param dataMap 数据
     * @param writer 不同的writer实现类处理不同的模板写入
     * @return
     */
    public Long exportExcel(Map<String, Object> dataMap,ExcelDataWriter writer){
        String fileName = writer.export(dataMap);
        String filePath = StrUtil.concat(true,FileUtil.getResourceBasePath(),TEMP_FILE_DIR,fileName);
        //文件上传逻辑自己实现
        //Long fileId = fileManagerService.uploadFile(filePath, fileName, "temp", ttl);
        //FileUtil.delDir(filePath);
        return 1234L;
    }

}
