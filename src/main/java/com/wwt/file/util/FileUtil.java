package com.wwt.file.util;

import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.wwt.file.enums.FileTypeEnum;

/**
 * 文件处理工具类
 */
public class FileUtil {

    /**
     * 获取项目根路径
     * pathStr : D:\code\javaCode\FileHandle\target\classes
     * 如果是在ide中运行，则和target同级目录,如果是jar部署到服务器，则默认和jar包同级
     * pathStr : D:\code\javaCode\FileHandle\         FileHandle是我的项目名
     */
    public static String getResourceBasePath() {
        ClassPathResource classPathResource = new ClassPathResource("");
        String pathStr = classPathResource.getFile().getAbsolutePath();
        pathStr = pathStr.replace("target\\classes", "");
        return pathStr;
    }

    /**
     * 如果父目录不存在，则创建
     * 保证父目录一定存在
     * 否则文件可能创建失败
     * 支持创建多级目录
     * @Param filePath 文件绝对路径
     */
    public static void mkParentDirIfNotExist(String filePath) {
        cn.hutool.core.io.FileUtil.mkParentDirs(filePath);
    }

    /**
     * 删除目录及其下文件
     * @param filePath 文件绝对路径
     */
    public static void delDir(String filePath){
        cn.hutool.core.io.FileUtil.del(filePath);
    }

    /**
     * 生成随机文件名
     * @param fileTypeEnum 指定文件类型
     * @return
     */
    public static String generateFileName(FileTypeEnum fileTypeEnum){
        if (ObjectUtil.isNull(fileTypeEnum)){
            return null;
        }
        //System.currentTimeMillis() 如果你不想用uuid，用时间也是可以的
        return StrUtil.concat(true, UUID.randomUUID().toString(true), fileTypeEnum.getSuffix());
    }

}
