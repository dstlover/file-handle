package com.wwt.file.util.excel;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.enums.WriteDirectionEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.alibaba.excel.write.metadata.fill.FillWrapper;
import com.wwt.file.entity.User;
import com.wwt.file.enums.UserTemplate;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户excel模板生成处理类
 */
@Slf4j
public class UserExcelDataWriter extends ExcelDataWriter{

    //指定模板名
    private final String template = UserTemplate.USER_TEMPLATE_EXCEL.getTemplate();

    /**
     * 初始化参数
     *
     */
    public UserExcelDataWriter() {
        super(UserTemplate.USER_TEMPLATE_EXCEL.getTemplate());
    }

    /**
     * 导出文件到本地
     * 返回文件的绝对路径
     * @param dataMap
     * @return
     */
    @Override
    public String export(Map<String, Object> dataMap) {
        User user1 = MapUtil.get(dataMap, "user1", User.class);
        User user2 = MapUtil.get(dataMap, "user2", User.class);
        List<User> user3 = MapUtil.get(dataMap, "users", List.class);
        List<User> user4 = MapUtil.get(dataMap, "users", List.class);
        List<User> user5 = MapUtil.get(dataMap, "users", List.class);
        String templateFileName = super.getTemplatePath();
        String filePath = super.getFilePath();

        ExcelWriter excelWriter = null;
        String fileName = null;
        try{
            //技巧，数据分段写入
            excelWriter = EasyExcel.write(filePath).withTemplate(templateFileName).build();
            WriteSheet writeSheet = EasyExcel.writerSheet().build();
            HashMap<String, Object> baseMessMap = MapUtil.newHashMap();
            HashMap<String, Object> user3ListMap = MapUtil.newHashMap();
            HashMap<String, Object> user4ListMap = MapUtil.newHashMap();

            //简单模板数据写入
            baseMessMap.put("name1",user1.getName());
            baseMessMap.put("age1",user1.getAge());
            baseMessMap.put("email1",user1.getEmail());
            baseMessMap.put("id1",user1.getId());

            baseMessMap.put("name2",user2.getName());
            baseMessMap.put("age2",user2.getAge());
            baseMessMap.put("email2",user2.getEmail());
            baseMessMap.put("id2",user2.getId());

            excelWriter.fill(baseMessMap, writeSheet);

            //横向列表数据写入
            FillConfig fillConfig = FillConfig.builder().direction(WriteDirectionEnum.HORIZONTAL).build();
            // 如果有多个list 模板上必须有{前缀.} 这里的前缀就是 data5，然后多个list必须用 FillWrapper包裹
            excelWriter.fill(new FillWrapper("user5", user5), fillConfig, writeSheet);
            //纵向列表数据写入 如果数据量很大，可以看看官网，有让你分段查询插入数据的办法，会用到缓存，效率还是很高的
            excelWriter.fill(new FillWrapper("user3", user3), writeSheet);
            excelWriter.fill(new FillWrapper("user4", user4), writeSheet);
            fileName = super.getFileName();
        }catch (Exception e){
            log.error("excel create error : ",e.getMessage(), e);
        }finally {
            if (ObjectUtil.isNotNull(excelWriter)){
                excelWriter.finish();
            }
        }
        return fileName;
    }
}
