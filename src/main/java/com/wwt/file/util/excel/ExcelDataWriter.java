package com.wwt.file.util.excel;

import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.util.StrUtil;
import com.wwt.file.enums.FileTypeEnum;
import com.wwt.file.util.FileUtil;
import lombok.Data;

import java.util.Map;

@Data
public abstract class ExcelDataWriter {

    /**
     * 模板名
     * template.xlsx
     * 子类构造方法去定义
     */
    private String template;

    /**
     * 模板所在目录路径
     * /xxx/xxx/xxx/target/classes/template/
     */
    private String tempBasePath;

    /**
     * 模板绝对路径
     * /xxx/xxx/xxx/target/classes/template/template.xlsx
     */
    private String templatePath;

    /**
     * 生成的文件名
     * aaa.xlsx
     */
    private String fileName;

    /**
     * 生成的文件的绝对路径
     * /xxx/xxx/xxx/aaa.xlsx
     */
    private String filePath;



    public abstract String export(Map<String, Object> dataMap);

    /**
     * 初始化参数
     */
    public ExcelDataWriter(String template) {
        //D:\code\javaCode\FileHandle\target\classes
        ClassPathResource classPathResource = new ClassPathResource("");
        String classPath = classPathResource.getFile().getAbsolutePath();

        this.template = template;
        //D:\code\javaCode\FileHandle\target\classes\template\
        this.tempBasePath =  StrUtil.concat(true,classPath,"\\template\\");
        //D:\code\javaCode\FileHandle\target\classes\template\xxx.xlsx
        this.templatePath = StrUtil.concat(true,tempBasePath,template);

        this.fileName = FileUtil.generateFileName(FileTypeEnum.EXCEL);
        //D:\code\javaCode\FileHandle\tmp\123456.xlsx
        this.filePath = StrUtil.concat(true,FileUtil.getResourceBasePath(),"tmp\\",this.fileName);
        //写数据前要保证父目录一定要存在
        FileUtil.mkParentDirIfNotExist(this.filePath);
    }


}
