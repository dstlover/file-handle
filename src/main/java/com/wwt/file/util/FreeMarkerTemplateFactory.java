package com.wwt.file.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;

import java.io.IOException;

/**
 * FreeMarker 创建模板对象的工厂
 */
public class FreeMarkerTemplateFactory {

    /**
     * FreeMarker的版本
     */
    private static final String FREEMARKER_VERSION = "2.3.28";

    /**
     * 模板存放目录
     */
    private static final String TEMPLATE_DIR = "/template";

    /**
     * 编码格式
     */
    private static final String CHARSET = "utf-8";


    /**
     * 获取模板对象
     * @param templateFileName
     * @return
     * @throws IOException
     */
    public static Template getTemplate(String templateFileName) throws IOException {
        Configuration configuration = new Configuration(new Version(FREEMARKER_VERSION));
        configuration.setDefaultEncoding("utf-8");
        //设置模板位置，默认是classpath下的指定目录下，idea中运行时为resources下的指定目录下
        configuration.setClassForTemplateLoading(DocUtil.class, TEMPLATE_DIR);
        Template template = configuration.getTemplate(templateFileName, CHARSET);
        return template;
    }

}
