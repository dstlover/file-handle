# FileHandle

## 说明
该项目是一个Spring boot项目，主要实现功能为文件预览，生成word、pdf、excel的后端。

## 使用前要先在resources下新建lib目录，然后下载一个生成pdf要用的jar包

### 下载jar包
地址： https://repository.aspose.com/repo/com/aspose/

![输入图片说明](https://foruda.gitee.com/images/1667727773899352364/770d3efc_7432475.png "屏幕截图")

### 然后把jar包放进lib目录下即可，jar包名要和pom中引入jar包的地方配置一致
![输入图片说明](tmp/aspose/libimage.png)
![输入图片说明](tmp/asposeimage.png)
